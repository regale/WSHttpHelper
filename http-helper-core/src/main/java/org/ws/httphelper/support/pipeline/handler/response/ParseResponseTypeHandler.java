package org.ws.httphelper.support.pipeline.handler.response;

import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ws.httphelper.common.Constant;
import org.ws.httphelper.core.pipeline.HandlerOrder;
import org.ws.httphelper.core.pipeline.ResponseHandler;
import org.ws.httphelper.exception.ResponseException;
import org.ws.httphelper.model.RequestContext;
import org.ws.httphelper.model.config.RequestConfig;
import org.ws.httphelper.model.http.ResponseData;
import org.ws.httphelper.model.http.ResponseFuture;
import org.ws.httphelper.model.http.ResponseType;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * 解析响应内容类型
 */
@HandlerOrder(value = Integer.MAX_VALUE,level = HandlerOrder.OrderLevel.SYSTEM)
public class ParseResponseTypeHandler implements ResponseHandler {

    private static Logger log = LoggerFactory.getLogger(ParseResponseTypeHandler.class.getName());

    private static final String REGEX_JSON = "(.*/json).*";
    private static final String REGEX_JAVASCRIPT = "(.*/javascript).*";
    private static final String REGEX_TEXT = ".*(text/plain).*";
    private static final String REGEX_HTML = "(.*/html).*";
    private static final String REGEX_XML = "(.*/xml).*";


    @Override
    public boolean handler(RequestContext requestContext) throws ResponseException {
        ResponseFuture responseFuture = requestContext.getResponseFuture();
        RequestConfig template = requestContext.getRequestConfig();
        if(responseFuture.isSuccess()){
            ResponseData response = responseFuture.getResponse();
            ResponseType responseType = template.getResponseType();
            Map<String, String> headers = response.getHeaders();
            if(MapUtils.isNotEmpty(headers)){
                String contentType = headers.get(Constant.HEAD_CONTENT_TYPE);
                if(contentType != null) {
                    if (Pattern.matches(REGEX_JSON, contentType)) {
                        responseType = ResponseType.JSON;
                    } else if (Pattern.matches(REGEX_HTML, contentType)) {
                        responseType = ResponseType.HTML;
                    } else if (Pattern.matches(REGEX_XML, contentType)) {
                        responseType = ResponseType.XML;
                    } else if (Pattern.matches(REGEX_TEXT, contentType)) {
                        responseType = ResponseType.TEXT;
                    } else if (Pattern.matches(REGEX_JAVASCRIPT, contentType)) {
                        responseType = ResponseType.JAVASCRIPT;
                    } else {
                        responseType = ResponseType.BINARY;
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("contentType:{} -> {}", contentType, responseType);
                    }
                }
            }
            responseFuture.setResponseType(responseType);
        }
        return true;
    }
}
