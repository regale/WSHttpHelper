package org.ws.httphelper.support.parser;

import org.ws.httphelper.core.parser.HtmlParser;
import org.ws.httphelper.exception.ParseException;
import org.ws.httphelper.model.field.ParseField;
import org.ws.httphelper.support.parser.field.FieldParser;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 从HTML中抽取数据的解析器实现
 */
public class HtmlObjectParser implements HtmlParser<Map<String, Object>> {

    /**
     * 字段解析器
     */
    private FieldParser fieldParser = new FieldParser();

    @Override
    public Map<String, Object> parse(final String html,final Collection<ParseField> parseFields) throws ParseException {
        Map<String, Object> map = new HashMap();
        // 遍历需要解析的字段定义
        for (ParseField field : parseFields) {
            String fieldName = field.getFieldName();
            // 使用字段解析器执行解析
            Object value = fieldParser.parse(html, field);
            // 对空格做处理
            if(value instanceof String){
                value = ((String) value).replaceAll("\\s+"," ");
            }
            map.put(fieldName,value);
        }
        return map;
    }
}
