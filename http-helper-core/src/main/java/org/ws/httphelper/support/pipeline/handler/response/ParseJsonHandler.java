package org.ws.httphelper.support.pipeline.handler.response;

import com.alibaba.fastjson.JSON;
import org.ws.httphelper.core.pipeline.HandlerOrder;
import org.ws.httphelper.core.pipeline.ResponseHandler;
import org.ws.httphelper.exception.ResponseException;
import org.ws.httphelper.model.RequestContext;
import org.ws.httphelper.model.http.ResponseData;
import org.ws.httphelper.model.http.ResponseFuture;
import org.ws.httphelper.model.http.ResponseType;

import java.util.Map;

@HandlerOrder(1)
public class ParseJsonHandler implements ResponseHandler {

    @Override
    public boolean handler(RequestContext requestContext) throws ResponseException {
        ResponseFuture responseFuture = requestContext.getResponseFuture();
        if (responseFuture == null){
            return true;
        }
        if(responseFuture.getResponseType() != ResponseType.JSON
                && responseFuture.getResponseType() != ResponseType.JAVASCRIPT){
            return true;
        }
        Class outputClass = requestContext.getOutputClass();
        // 默认转化成map
        if(outputClass == null){
            outputClass = Map.class;
        }
        if(responseFuture.isSuccess()){
            ResponseData response = responseFuture.getResponse();
            String body = (String) response.getBody();
            Object output = JSON.parseObject(body, outputClass);
            responseFuture.setOutput(output);
        }
        return true;
    }

}
