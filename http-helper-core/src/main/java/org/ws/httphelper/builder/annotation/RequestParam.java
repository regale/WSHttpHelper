package org.ws.httphelper.builder.annotation;

import org.ws.httphelper.model.field.ParamFieldType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 请求参数
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestParam {

    String fieldName() default "";

    String defaultValue() default "";

    boolean required() default false;

    ParamFieldType fieldType() default ParamFieldType.STRING;

    String validation() default "";

    String description() default "";

    String example() default "";

}
