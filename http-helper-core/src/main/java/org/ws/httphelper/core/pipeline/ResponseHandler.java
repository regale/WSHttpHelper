package org.ws.httphelper.core.pipeline;

import org.ws.httphelper.exception.ResponseException;
import org.ws.httphelper.model.RequestContext;

/**
 * 响应处理器
 */
public interface ResponseHandler extends HttpHandler {

    /**
     * 响应后处理.
     * @param requestContext 响应回调
     * @return true:成功;false:失败，结束后续处理;
     * @throws ResponseException 后处理异常
     */
    boolean handler(RequestContext requestContext) throws ResponseException;

}
