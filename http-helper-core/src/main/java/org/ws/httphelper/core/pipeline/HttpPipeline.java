package org.ws.httphelper.core.pipeline;

import java.util.concurrent.ExecutorService;

/**
 * 包含一组Http处理器的Pipeline
 */
public interface HttpPipeline {
    HttpPipeline addFirst(HttpHandler handler);
    HttpPipeline addFirst(HttpHandler... handlers);

    HttpPipeline addFirst(ExecutorService executor, HttpHandler handler);
    HttpPipeline addFirst(ExecutorService executor, HttpHandler... handlers);

    HttpPipeline addLast(HttpHandler handler);
    HttpPipeline addLast(HttpHandler... handlers);

    HttpPipeline addLast(ExecutorService executor, HttpHandler handler);
    HttpPipeline addLast(ExecutorService executor, HttpHandler... handlers);

    HttpPipeline remove(Class<? extends HttpHandler> handlerType);
}
