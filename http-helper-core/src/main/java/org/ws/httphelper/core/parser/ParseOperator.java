package org.ws.httphelper.core.parser;

import org.ws.httphelper.model.field.ParseField;

import java.util.List;
import java.util.Map;

/**
 * 描述: 解析操作器
 *
 * @author gz
 * @create 2018-11-09 10:07
 */
public interface ParseOperator {

    /**
     * 获取Sting数据
     * @param body
     * @param fieldInfo
     * @return
     */
    String parseString(String body, ParseField fieldInfo)throws Exception;

    /**
     * 获取列表
     * @param body
     * @param listFieldInfo
     * @return
     */
    List parseList(String body, ParseField listFieldInfo)throws Exception;

    /**
     *
     * @param body
     * @param mapFieldInfo
     * @return
     */
    Map parseMap(String body, ParseField mapFieldInfo)throws Exception;
}
