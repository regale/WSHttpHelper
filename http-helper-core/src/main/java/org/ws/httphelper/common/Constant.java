package org.ws.httphelper.common;

public interface Constant {

    String URL_PATTERN = "^(https?|ftp|file):\\/\\/[-a-zA-Z0-9+&@#\\/%?=~_|!:,.;\\{\\}]*[-a-zA-Z0-9+&@#\\/%=~_|\\{\\}]";

    String HEAD_CONTENT_TYPE = "Content-Type";

    String HEAD_CONTENT_DISPOSITION = "Content-Disposition";

    String UTF_8 = "UTF-8";


}
