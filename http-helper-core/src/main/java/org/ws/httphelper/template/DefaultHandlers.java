package org.ws.httphelper.template;

import org.ws.httphelper.core.pipeline.HttpHandler;
import org.ws.httphelper.core.pipeline.RequestHandler;
import org.ws.httphelper.core.pipeline.ResponseHandler;
import org.ws.httphelper.support.pipeline.handler.request.BuildBodyHandler;
import org.ws.httphelper.support.pipeline.handler.request.BuildHeaderHandler;
import org.ws.httphelper.support.pipeline.handler.request.BuildParamHandler;
import org.ws.httphelper.support.pipeline.handler.request.BuildUrlHandler;
import org.ws.httphelper.support.pipeline.handler.request.ValidationParamHandler;
import org.ws.httphelper.support.pipeline.handler.response.ParseHtmlHandler;
import org.ws.httphelper.support.pipeline.handler.response.ParseJsonHandler;
import org.ws.httphelper.support.pipeline.handler.response.ParseResponseTypeHandler;
import org.ws.httphelper.support.pipeline.handler.response.ParseXmlHandler;

/**
 * 默认Handler
 */
public class DefaultHandlers {

    private DefaultHandlers() {

    }

    /**
     * 所有默认的请求处理器RequestHandler
     * @return
     */
    public static RequestHandler[] requestHandlers(){
        return new RequestHandler[]{
                new ValidationParamHandler(),
                new BuildUrlHandler(),
                new BuildHeaderHandler(),
                new BuildBodyHandler(),
                new BuildParamHandler()
        };
    }

    /**
     * 所有默认的响应处理器ResponseHandler
     * @return
     */
    public static ResponseHandler[] responseHandlers(){
        return new ResponseHandler[]{
                new ParseResponseTypeHandler(),
                new ParseHtmlHandler(),
                new ParseXmlHandler(),
                new ParseJsonHandler()
        };
    }

    /**
     * 全部的请求和响应处理器
     * @return
     */
    public static HttpHandler[] allHandlers(){
        RequestHandler[] requestHandlers = requestHandlers();
        ResponseHandler[] responseHandlers = responseHandlers();
        HttpHandler[] handlers = new HttpHandler[requestHandlers.length+ responseHandlers.length];
        for (int i = 0; i < requestHandlers.length; i++) {
            handlers[i] = (HttpHandler)requestHandlers[i];
        }
        for (int i = 0,j=requestHandlers.length; i < responseHandlers.length; i++,j++) {
            handlers[j] = (HttpHandler)responseHandlers[i];
        }
        return handlers;
    }

}
