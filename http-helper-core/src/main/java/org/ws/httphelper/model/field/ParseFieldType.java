package org.ws.httphelper.model.field;

/**
 * 支持解析的字段类型：String
 */
public enum ParseFieldType {
    STRING, LIST, OBJECT
}