package org.ws.httphelper.model.field;

public enum ParamFieldType {
    // 字符串
    STRING,
    // 数字
    NUMBER,
    // 列表：可以传输多个
    LIST,
    // 文件
    FILE;
}
