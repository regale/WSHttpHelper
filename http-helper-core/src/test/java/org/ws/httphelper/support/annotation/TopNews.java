package org.ws.httphelper.support.annotation;

import org.ws.httphelper.builder.annotation.ResponseEntity;
import org.ws.httphelper.builder.annotation.ResponseField;
import org.ws.httphelper.model.field.ExpressionType;
import org.ws.httphelper.model.field.ParseFieldType;

import java.util.List;

@ResponseEntity
public class TopNews {

    @ResponseField(fieldName = "newsItems",fieldType = ParseFieldType.LIST,
            parseExpression = "//table[contains(@class,'list-table')]/tbody/tr",expressionType = ExpressionType.XPATH)
    private List<NewsItem> newsItems;

    public List<NewsItem> getNewsItems() {
        return newsItems;
    }

    public void setNewsItems(List<NewsItem> newsItems) {
        this.newsItems = newsItems;
    }
}
