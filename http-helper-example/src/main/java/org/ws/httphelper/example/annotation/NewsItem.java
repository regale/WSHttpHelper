package org.ws.httphelper.example.annotation;

import org.ws.httphelper.builder.annotation.ResponseEntity;
import org.ws.httphelper.builder.annotation.ResponseField;
import org.ws.httphelper.model.field.ExpressionType;
import org.ws.httphelper.model.field.ParseFieldType;

@ResponseEntity
public class NewsItem {

    @ResponseField(fieldType = ParseFieldType.STRING,parseExpression = "./td[contains(@class,'first')]",expressionType = ExpressionType.XPATH)
    private String number;

    @ResponseField(fieldType = ParseFieldType.STRING,parseExpression = ".//a[contains(@class,'list-title')]",expressionType = ExpressionType.XPATH)
    private String title;

    @ResponseField(fieldType = ParseFieldType.STRING,parseExpression = "./td[contains(@class,'tc')]/a[1]/@href",expressionType = ExpressionType.XPATH)
    private String newsLink;

    @ResponseField(fieldType = ParseFieldType.STRING,parseExpression = "./td[contains(@class,'tc')]/a[2]/@href",expressionType = ExpressionType.XPATH)
    private String videoLink;

    @ResponseField(fieldType = ParseFieldType.STRING,parseExpression = "./td[contains(@class,'tc')]/a[3]/@href",expressionType = ExpressionType.XPATH)
    private String imageLink;

    @ResponseField(fieldType = ParseFieldType.STRING,parseExpression = "./td[contains(@class,'last')]",expressionType = ExpressionType.XPATH)
    private String last;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNewsLink() {
        return newsLink;
    }

    public void setNewsLink(String newsLink) {
        this.newsLink = newsLink;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }
}
