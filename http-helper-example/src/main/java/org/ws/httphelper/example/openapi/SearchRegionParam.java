package org.ws.httphelper.example.openapi;

import org.ws.httphelper.builder.annotation.IgnoreField;
import org.ws.httphelper.builder.annotation.RequestEntity;
import org.ws.httphelper.builder.annotation.RequestParam;

@RequestEntity
public class SearchRegionParam {

    @RequestParam(fieldName = "query",required = true,example = "天安门、美食",description = "检索关键字")
    private String query;

    @RequestParam(fieldName = "tag",example = "美食",description = "检索分类偏好")
    private String tag;

    @RequestParam(fieldName = "region",required = true,example = "北京、131（北京的code）、海淀区、全国，等",description = "检索行政区划区域")
    private String region;

    @RequestParam(fieldName = "city_limit",defaultValue = "true",example = "true、false",description = "区域数据召回限制，为true时，仅召回region对应区域内数据。")
    private String cityLimit;

    @RequestParam(fieldName = "extensions_adcode",defaultValue = "true",example = "true、false",description = "是否召回国标行政区划编码，")
    private String extensionsAdcode;

    @RequestParam(fieldName = "output",defaultValue = "json",example = "json或xml",description = "输出格式为json或者xml")
    private String output;

    @RequestParam(fieldName = "scope",defaultValue = "1",example = "1、2",description = "检索结果详细程度。取值为1 或空，则返回基本信息；取值为2，返回检索POI详细信息")
    private String scope;

    @RequestParam(fieldName = "filter",example = "sort_name:distance|sort_rule:1",description = "检索过滤条件。当scope取值为2时，可以设置filter进行排序。")
    private String filter;

    @RequestParam(fieldName = "coord_type",defaultValue = "3",example = "1、2、3(默认)、4",description = "坐标类型，1（wgs84ll即GPS经纬度），2（gcj02ll即国测局经纬度坐标），3（bd09ll即百度经纬度坐标），4（bd09mc即百度米制坐标）")
    private int coordType;

    @RequestParam(fieldName = "ret_coordtype",example = "gcj02ll",description = "可选参数，添加后POI返回国测局经纬度坐标")
    private String retCoordtype;

    @RequestParam(fieldName = "page_size",defaultValue = "10",example = "10",description = "单次召回POI数量，默认为10条记录，最大返回20条。多关键字检索时，返回的记录数为关键字个数*page_size。")
    private int pageSize;

    @RequestParam(fieldName = "page_num",defaultValue = "0",example = "0、1、2",description = "分页页码，默认为0,0代表第一页，1代表第二页，以此类推。")
    private int pageNum;

    @RequestParam(fieldName = "ak",required = true,description = "开发者的访问密钥，必填项。v2之前该属性为key。")
    private String ak;

    @RequestParam(fieldName = "sn",description = "开发者的权限签名。")
    private String sn;

    @RequestParam(fieldName = "timestamp",description = "设置sn后该值必填。")
    private String timestamp;

    @IgnoreField
    private String sk;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCityLimit() {
        return cityLimit;
    }

    public void setCityLimit(String cityLimit) {
        this.cityLimit = cityLimit;
    }

    public String getExtensionsAdcode() {
        return extensionsAdcode;
    }

    public void setExtensionsAdcode(String extensionsAdcode) {
        this.extensionsAdcode = extensionsAdcode;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public int getCoordType() {
        return coordType;
    }

    public void setCoordType(int coordType) {
        this.coordType = coordType;
    }

    public String getRetCoordtype() {
        return retCoordtype;
    }

    public void setRetCoordtype(String retCoordtype) {
        this.retCoordtype = retCoordtype;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public String getAk() {
        return ak;
    }

    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSk() {
        return sk;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }
}
