package org.ws.httphelper.example.rest;

import org.ws.httphelper.builder.annotation.Get;
import org.ws.httphelper.builder.annotation.HttpClient;
import org.ws.httphelper.builder.annotation.HttpOperation;
import org.ws.httphelper.builder.annotation.HttpRequest;
import org.ws.httphelper.model.UserEntity;
import org.ws.httphelper.model.http.ClientType;
import org.ws.httphelper.model.http.ResponseFuture;

import java.util.Map;
import java.util.function.Consumer;

/**
 * 根据org.ws.httphelper.api.TestApiController定义的访问接口用于测试
 */
@HttpClient(clientType = ClientType.OK_HTTP_CLIENT)
@HttpRequest(rootUrl = "http://127.0.0.1:8888/test/api")
@HttpOperation
public interface TestMethodApi {

    @Get("/get")
    ResponseFuture getSync(UserEntity userEntity);

    @Get("/get")
    Map<String,String> getMapSync(UserEntity userEntity,Class outputClass);

    /**
     * 异步
     * @param userEntity
     * @param callback
     */
    @HttpClient(threadNumber = 100)
    @Get("/get")
    void getAsync(UserEntity userEntity, Consumer<ResponseFuture> callback);

}
