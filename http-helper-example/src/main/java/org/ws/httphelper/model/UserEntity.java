package org.ws.httphelper.model;

import lombok.Data;

@Data
public class UserEntity {

    private String name;
    private String six;
    private int age;
}
