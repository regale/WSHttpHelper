package org.ws.httphelper.spring;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.ws.httphelper.model.config.ClientConfig;
import org.ws.httphelper.spring.model.RequestProperties;

@ConfigurationProperties(prefix = "http-helper",ignoreUnknownFields = true)
public class HttpHelperProperties {

    private String defaultTemplate;

    private ClientConfig client;

    private RequestProperties request;

    public String getDefaultTemplate() {
        return defaultTemplate;
    }

    public void setDefaultTemplate(String defaultTemplate) {
        this.defaultTemplate = defaultTemplate;
    }

    public ClientConfig getClient() {
        return client;
    }

    public void setClient(ClientConfig client) {
        this.client = client;
    }

    public RequestProperties getRequest() {
        return request;
    }

    public void setRequest(RequestProperties request) {
        this.request = request;
    }
}
