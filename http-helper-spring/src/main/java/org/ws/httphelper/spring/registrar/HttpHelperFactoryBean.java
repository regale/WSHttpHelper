package org.ws.httphelper.spring.registrar;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.Assert;
import org.ws.httphelper.builder.HttpHelperBuilder;

public class HttpHelperFactoryBean implements FactoryBean<Object>, InitializingBean , ApplicationContextAware {

    private ApplicationContext applicationContext;
    private Class<?> type;

    public HttpHelperFactoryBean() {
    }

    public HttpHelperFactoryBean(Class type) {
        this.type = type;
    }

    @Override
    public Object getObject() throws Exception {
        return HttpHelperBuilder.builder().builderByAnn(this.type);
    }

    @Override
    public Class<?> getObjectType() {
        return this.type;
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(type,"Type must be set.");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext.getParent();
    }
}
